// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// Event Event table / model
// swagger:model Event
type Event struct {

	// banner URL img1
	BannerURLImg1 string `json:"banner_url_img1,omitempty"`

	// banner URL img2
	BannerURLImg2 string `json:"banner_url_img2,omitempty"`

	// contact person email
	ContactPersonEmail string `json:"contact_person_email,omitempty"`

	// contact person name
	ContactPersonName string `json:"contact_person_name,omitempty"`

	// contact person phone
	ContactPersonPhone string `json:"contact_person_phone,omitempty"`

	// date created
	// Format: date-time
	DateCreated strfmt.DateTime `json:"date_created,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	// event end date
	EventEndDate string `json:"event_end_date,omitempty"`

	// event start date
	EventStartDate string `json:"event_start_date,omitempty"`

	// ID
	ID int64 `json:"id,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// status
	Status int64 `json:"status,omitempty"`
}

// Validate validates this event
func (m *Event) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateDateCreated(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Event) validateDateCreated(formats strfmt.Registry) error {

	if swag.IsZero(m.DateCreated) { // not required
		return nil
	}

	if err := validate.FormatOf("date_created", "body", "date-time", m.DateCreated.String(), formats); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *Event) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Event) UnmarshalBinary(b []byte) error {
	var res Event
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
