// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// EventRequestHandlerFunc turns a function with the right signature into a event request handler
type EventRequestHandlerFunc func(EventRequestParams, *ticketboss) middleware.Responder

// Handle executing the request and returning a response
func (fn EventRequestHandlerFunc) Handle(params EventRequestParams, principal *ticketboss) middleware.Responder {
	return fn(params, principal)
}

// EventRequestHandler interface for that can handle valid event request params
type EventRequestHandler interface {
	Handle(EventRequestParams, *ticketboss) middleware.Responder
}

// NewEventRequest creates a new http.Handler for the event request operation
func NewEventRequest(ctx *middleware.Context, handler EventRequestHandler) *EventRequest {
	return &EventRequest{Context: ctx, Handler: handler}
}

/*EventRequest swagger:route POST /events Events event eventRequest

Create a new Event.

JSON Response is provided with respective success or fail details.

*/
type EventRequest struct {
	Context *middleware.Context
	Handler EventRequestHandler
}

func (o *EventRequest) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewEventRequestParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal *ticketboss
	if uprinc != nil {
		principal = uprinc.(*ticketboss) // this is really a ticketboss, I promise
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
