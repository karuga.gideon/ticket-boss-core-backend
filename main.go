// Package classification Ticket Boss API.
//
// This is the core application that powers ticket boss ticketing sytem.
//
// Terms Of Service:
//
// Terms of service as https://ticketboss.com/tos
//
//     Schemes: http, https
//     Host: 176.58.124.136:9020
//     BasePath: /ticketboss
//     Version: 0.0.1
//     Contact: Gideon Karuga<karuga.gideon@gmail.com> http://strutstechnology.co.ke
//
//     Consumes:
//     - application/json
//     - application/xml
//
//     Produces:
//     - application/json
//     - application/xml
//
//     Security:
//     - api_key:
//
//     SecurityDefinitions:
//     api_key:
//          type: apiKey
//          name: KEY
//          in: header
//     Extensions:
//     x-meta-value: value
//     x-meta-array:
//       - value1
//       - value2
//     x-meta-array-obj:
//       - name: obj
//         value: field
//
// swagger:meta
package main

import (
	"ticket-boss/app"
	"ticket-boss/config"
)

func main() {

	config := config.GetConfig()
	app := &app.App{}
	app.Initialize(config)
	app.Run(":9020")

}
