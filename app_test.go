package main

import (
	"log"
	"os"
	"testing"

	// "save-ussd-go-v2/app"
	"save-ussd-go-v2/app/utils"
)

func TestConfig(t *testing.T) {
	log.Println("Testing add function...")
	result := utils.Add(1, 2)
	log.Println("1 + 2 = ", result)
	if result != 3 {
		t.Error("Error adding up ints, expected output of 1 + 2 was expected to be 3.")
	}
}

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	os.Exit(m.Run())
}

/**
go test
go test -run ''      # Run all tests.
go test -run Foo     # Run top-level tests matching "Foo", such as "TestFooBar".
go test -run Foo/A=  # For top-level tests matching "Foo", run subtests matching "A=".
go test -run /A=1    # For all top-level tests, run subtests matching "A=1".

**/
