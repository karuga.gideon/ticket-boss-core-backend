package config

import (
	"encoding/json"
	"log"
	"os"
)

// Config - Map json config to local json object
type Config struct {
	Database struct {
		Dialect  string `json:"db_dialect"`
		Username string `json:"db_username"`
		Password string `json:"db_password"`
		Name     string `json:"db_name"`
		Charset  string `json:"db_charset"`
		Host     string `json:"db_host"`
		Sslmode  string `json:"db_sslmode"`
	} `json:"database"`
}

// LoadConfiguration  - Read settings / config file and load settings to json
func LoadConfiguration(file string) Config {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		log.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

// GetConfig - read from settings / json file
func GetConfig() Config {
	config := LoadConfiguration("config.json")
	return config
}
