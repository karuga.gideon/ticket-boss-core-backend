# ticket-boss REST API

A RESTful API ticket-boss with Go

It is just a RESTful API with Go using **gorilla/mux** (A nice mux library) and **gorm** (An ORM for Go)

## Installation & Run

```bash
# Download this project
go get github.com/Exuus/ticket-boss
```

Before running API server, you should set the database config with yours or set the your database config with my values on [config.go](https://github.com/Exuus/ticket-boss/blob/master/config/config.go)

```go
func GetConfig() *Config {
	return &Config{
		DB: &DBConfig{
			Dialect:  "postgres",
			Username: "pgadmin",
			Password: "openpgpwd",
			Name:     "payment_gateway",
			Charset:  "utf8",
			Host:  "localhost",
			Sslmode:  "disable",
		},
	}
}
```

```bash
# Build and Run
cd ticket-boss
go build
./ticket-boss

# API Endpoint : http://127.0.0.1:3000
```

## Structure

```
├── app
│   ├── app.go
│   ├── handler          // Our API core handlers
│   │   ├── common.go    // Common response functions
│   │   ├── organization.go  // APIs for Project model
│   │   └── transaction.go     // APIs for Task model
│   └── model
│       └── model.go     // Models for our application
├── config
│   └── config.go        // Configuration
└── main.go
```

## API

#### /projects

- `GET` : Get all projects
- `POST` : Create a new project

#### /projects/:title

- `GET` : Get a project
- `PUT` : Update a project
- `DELETE` : Delete a project

#### /projects/:title/archive

- `PUT` : Archive a project
- `DELETE` : Restore a project

#### /projects/:title/tasks

- `GET` : Get all tasks of a project
- `POST` : Create a new task in a project

#### /projects/:title/tasks/:id

- `GET` : Get a task of a project
- `PUT` : Update a task of a project
- `DELETE` : Delete a task of a project

#### /projects/:title/tasks/:id/complete

- `PUT` : Complete a task of a project
- `DELETE` : Undo a task of a project

## Todo

- [x] Support basic REST APIs.
- [ ] Support Authentication with user for securing the APIs.
- [ ] Make convenient wrappers for creating API handlers.
- [ ] Write the tests for all APIs.
- [x] Organize the code with packages
- [ ] Make docs with GoDoc
- [ ] Building a deployment process




## Server API Requests

## Authentication
## POST Request

http://localhost:8089/authenticate

{
	"username":"username",
	"password":"password"
}

## Response

{
    "status": "00",
    "token": "jwt.token"
}

## All other requests sent With Authoirzation token in header

## Create Transaction
## MTN

## Debit Transaction / Cash In [POST]

http://localhost:8089/transactions/debit

## MTN

{
	"msisdn":"250781534563",
	"account":"250781534563",
	"amount":"10",
	"description":"Test Debit - Cash In",
	"provider":"MTN",
	"trx_id": "TESTTRX102400802"
}


## TIGO

{
	"msisdn":"250727116186",
	"account":"250727116186",
	"amount":"1",
	"description":"Test Debit - Cash In",
	"provider":"TIGO",
	"trx_id": "TESTTRX102400901"
}



## Credit Transaction / Cash Out
## Post Request  

http://localhost:8089/transactions/credit

## MTN

{
	"msisdn":"250781534563",
	"account":"250781534563",
	"amount":"1",
	"description":"Test Credit",
	"provider":"MTN",
	"trx_id": "TESTTRX102400802"
}

## TIGO

{
	"msisdn":"250781534563",
	"account":"250781534563",
	"amount":"1",
	"description":"Test Credit",
	"provider":"MTN",
	"trx_id": "TESTTRX102400802"
}

# Sample Response

{
    "id": 39,
    "sender": "",
    "receiver": "",
    "msisdn": "250727116186",
    "amount": "1",
    "provider": "TIGO",
    "account": "250727116186",
    "description": "Test Credit Transaction",
    "time_initiated": "0001-01-01T00:00:00Z",
    "time_completed": "0001-01-01T00:00:00Z",
    "status": "OK",
    "trx_id": "TESTTRX102510814",
    "third_party_trxid": "5452787005"
}

## Transaction status [GET]
# Transaction status for Tigo is available only for cash in transactions.

http://localhost:8089/transaction/status/TESTTRX102400801



## For Admin Portal

## Count transactions [GET]
http://localhost:8089/transactions/count


## View all transactions [GET]
http://localhost:8089/transactions


## View all organizations [GET]
http://localhost:8089/organizations


## Organizations Count [GET]
http://localhost:8089/organizations/count

## Create Organization [POST]

http://localhost:8089/organizations

#Payload

{
	"organization_name":"Catrix",
	"contact_person":"Alleluia Exuus",
	"phone_number":"250781534563"
}


# New user creation [POST]
http://localhost:8089/users

# Payload
# To note, the org id in user creation is an int value.

{
	"name":"John Doe",
	"email":"jd@gmail.com",
	"username":"john_doe",
	"password":"809819232kjlkjljl2j323j999",
	"org_id": 2
}

# Users Count [GET]

http://localhost:8089/users/count


# Random password generation [GET]
http://localhost:8089/users/generate_pwd


# User password reset [POST]
http://localhost:8089/users/reset_pwd/8

{
	"password":"dc0e98856858f884f6dfc863fb4bcff81e27"
}
