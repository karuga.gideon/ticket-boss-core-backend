package app

import (
	"log"
	"net/http"

	// "github.com/rs/cors"
	"ticket-boss/app/handler"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"ticket-boss/config"

	raven "github.com/getsentry/raven-go"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	// MySQL Lib import
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// App has router and db instances
type App struct {
	Router *mux.Router
	DB     *gorm.DB
	Config *config.Config
}

// Initialize initializes the app with predefined configuration
func (a *App) Initialize(config config.Config) {

	raven.SetDSN("https://4721f61661b945cd97dc2eeea0230658:6aecf01aeaa6470b91b7b9e4d9ffd9da@sentry.io/243952-STRUTSRAVEN")

	// Connect to ticket-boss Database
	mysqlConnection := config.Database.Username + ":" + config.Database.Password + "@/" + config.Database.Name + "?charset=utf8&parseTime=True&loc=Local"
	log.Println("MySql Db Connection String >>>  : ", mysqlConnection)
	// db, err := gorm.Open("mysql", "user:password@/dbname?charset=utf8&parseTime=True&loc=Local")
	db, err := gorm.Open("mysql", mysqlConnection)
	// saveUssdDB, err := sql.Open("mysql", mysqlConnection)
	utils.CheckError(err, "TicketBoss MySql DB Connection")

	a.DB = model.DBMigrate(db)

	router := mux.NewRouter()

	// SWAGGER-UI
	swaggerUIDir := "/swaggerui/"
	router.
		PathPrefix(swaggerUIDir).
		Handler(http.StripPrefix(swaggerUIDir, http.FileServer(http.Dir("."+swaggerUIDir))))
	a.Router = router

	a.setRouters()

	// CheckPendingTransactionsStatus - Check Trx Status
	// go scheduler.CheckPendingTransactionsStatus(a.DB)

	// Check for unsent messages to send
	go handler.CheckForSysMsgsToSend(config, a.DB)

	// Send stat reports via email
	// go handler.SendHourlyStats(config, a.DB)

}

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

// setRouters sets the all required routers
func (a *App) setRouters() {

	// a.Get("/ticketboss/swaggerui", a.SwaggerUI)
	// a.Get("/ticketboss/swaggerui/", a.SwaggerUI)

	// EVENTS
	// Create Event
	a.Post("/ticketboss/events", a.CreateEvent)
	a.Post("/ticketboss/events/", a.CreateEvent)

	// View all events - GetAllEvents
	a.Get("/ticketboss/events", a.GetAllEvents)
	a.Get("/ticketboss/events/", a.GetAllEvents)

	// GetSingleEvent - Get Single Event details
	a.Get("/ticketboss/events/{id}", a.GetSingleEvent)
	a.Get("/ticketboss/events/{id}/", a.GetSingleEvent)

	// Create Ticket Types
	a.Post("/ticketboss/events/{id}/tickettypes", a.CreateTicketType)
	a.Post("/ticketboss/events/{id}/tickettypes/", a.CreateTicketType)

	// USERS
	// Create User
	a.Post("/ticketboss/users", a.RegisterUser)
	a.Post("/ticketboss/users/", a.RegisterUser)

	// User Login - UserLogin
	a.Get("/ticketboss/users", a.UserLogin)
	a.Get("/ticketboss/users/", a.UserLogin)

	// STATISTICS
	a.Get("/ticketboss/statistics", a.Statistics)
	a.Get("/ticketboss/statistics/", a.Statistics)

}

// CreateEvent - CreateEvent
func (a *App) CreateEvent(w http.ResponseWriter, r *http.Request) {
	handler.CreateEvent(a.DB, w, r)
}

// GetAllEvents - GetAllEvents
func (a *App) GetAllEvents(w http.ResponseWriter, r *http.Request) {
	handler.GetAllEvents(a.DB, w, r)
}

// GetSingleEvent - GetSingleEvent
func (a *App) GetSingleEvent(w http.ResponseWriter, r *http.Request) {
	handler.GetSingleEvent(a.DB, w, r)
}

// CreateTicketType - CreateTicketType
func (a *App) CreateTicketType(w http.ResponseWriter, r *http.Request) {
	handler.CreateTicketType(a.DB, w, r)
}

// RegisterUser - RegisterUser
func (a *App) RegisterUser(w http.ResponseWriter, r *http.Request) {
	handler.RegisterUser(a.DB, w, r)
}

// UserLogin - UserLogin
func (a *App) UserLogin(w http.ResponseWriter, r *http.Request) {
	handler.UserLogin(a.DB, w, r)
}

// Statistics - Statistics
func (a *App) Statistics(w http.ResponseWriter, r *http.Request) {
	handler.Statistics(a.DB, w, r)
}

// SwaggerUI - SwaggerUI
func (a *App) SwaggerUI(w http.ResponseWriter, r *http.Request) {
	// ru := a.Router
	// sh := http.StripPrefix("/swaggerui/", http.FileServer(http.Dir("./swaggerui/")))
	// ru.PathPrefix("/swaggerui/").Handler(sh)

	// swaggerUIFiles := template.Must(template.ParseGlob("./swaggerui/*.*"))
	// swaggerUIFiles.ExecuteTemplate(w, "index.html", nil)

	// http.ServeFile(w, r, r.URL.Path)
	// http.FileServer(http.Dir("swaggerui/"))

	// router := httpRouter.New()
	// router.ServeFiles("/swaggerui/*filepath", http.Dir("./swaggerui/"))

	// fs := http.FileServer(http.Dir("./swaggerui"))
	// http.Handle("/swaggerui/", http.StripPrefix("/swaggerui/", fs))
}

// Run the app on it's router
// go-swagger examples.
//
// The purpose of this application is to provide some
// use cases describing how to generate docs for your API
//
//     Schemes: http, https
//     Host: localhost
//     BasePath: /
//     Version: 0.0.1
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
func (a *App) Run(host string) {

	// Cors configuration
	headers := handlers.AllowedHeaders([]string{
		"X-Requested-With", "Content-Type", "Authorization",
		"Access-Control-Allow-Origin", "http://localhost:3000",
		"Access-Control-Allow-Origin", "*"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"})
	origins := handlers.AllowedHeaders([]string{"*"})
	// (w).Header().Set("Access-Control-Allow-Origin", "*")

	// fs := http.FileServer(http.Dir("./swaggerui"))
	// a.Router.PathPrefix("/swaggerui").Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 	if r.URL.Path == "/swaggerui" {
	// 		//your default page
	// 		r.URL.Path = "/index.html"
	// 	}

	// 	fs.ServeHTTP(w, r)
	// })).Methods("GET")

	log.Println("Application started on port >>> ", host)
	// log.Fatal(http.ListenAndServe(host, a.Router)) - how it was before cors configuration
	log.Fatal(http.ListenAndServe(host, handlers.CORS(headers, methods, origins)(a.Router)))

}
