package handler

import (
	"log"
	"net/http"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"ticket-boss/config"

	"github.com/jinzhu/gorm"
)

// Statistics - Return current statistics
// swagger:route GET /statistics Statistics statistics
// Return current statistics.
// Statistics on number of events, registrations, transaction cound and sum.
// responses:
//  200: statisticsReport
func Statistics(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	// Total events, customer registrations, total transactions count and amount
	stats := model.StatisticsReport{}

	stats.TotalEvents = GetTotalEventsCount(db)
	stats.TotalMembers = GetTotalRegisteredCustomersCount(db)
	stats.TotalTransactionsCount = GetTotalTransactionsCount(db)
	stats.TotalTransactionsAmount = GetTotalTransactionsSum(db)

	respondJSON(w, http.StatusOK, stats)
}

// SendHourlyStats - SendHourlyStats
func SendHourlyStats(config config.Config, db *gorm.DB) {

	for i := 0; ; i++ {
		log.Println("Sending hourly stats...")
		CreateStatsEmail(db, "karuga.gideon@gmail.com")
		utils.DelaySeconds(3600) // Check every 1 hour
	}
}
