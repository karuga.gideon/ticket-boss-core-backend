package handler

import (
	"net/http"
	"ticket-boss/app/model"

	"github.com/jinzhu/gorm"
)

// GetAllTranstactions - GetAllTranstactions
func GetAllTranstactions(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	transactions := []model.Transaction{}
	db.Order("ID ASC").Find(&transactions)
	respondJSON(w, http.StatusOK, transactions)
}

// GetTotalTransactionsCount - GetTotalTransactionsCount
func GetTotalTransactionsCount(db *gorm.DB) float64 {
	var result model.Result
	// # 0 Male # 1 Female
	db.Raw("select count(*) as total from transactions").Scan(&result)
	return result.Total
}

// GetTotalTransactionsSum - GetTotalTransactionsSum
func GetTotalTransactionsSum(db *gorm.DB) float64 {
	var result model.Result
	// # 0 Male # 1 Female
	db.Raw("select sum(amount) as total from transactions").Scan(&result)
	return result.Total
}

// GetTransactionDetails - GetTransactionDetails
func GetTransactionDetails(db *gorm.DB, transactionID int) *model.Transaction {
	transaction := model.Transaction{}
	db.Where("id = ?", transactionID).Find(&transaction).First(&transaction)
	return &transaction
}
