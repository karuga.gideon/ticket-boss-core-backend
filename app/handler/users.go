package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"time"

	"github.com/jinzhu/gorm"
)

// passphrase for passwords encryption
var passphrase = "USUCitxcwDMONwYuZMRyEwOMuzXNSrGpuzvdvUrgWxgV29h2"

// GetAllUsers - GetAllUsers
func GetAllUsers(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	users := []model.User{}
	db.Order("ID ASC").Find(&users)
	respondJSON(w, http.StatusOK, users)
}

// GetTotalRegisteredCustomersCount - GetTotalRegisteredCustomersCount
func GetTotalRegisteredCustomersCount(db *gorm.DB) float64 {
	var result model.Result
	// # 0 Male # 1 Female
	db.Raw("select count(*) as total from users where type = ? ", 2).Scan(&result)
	return result.Total
}

// RegisterUser - Member Sign Up
// swagger:route POST /users Users userRequest
// Create a new User.
// JSON Response is provided with respective success or fail details.
// responses:
//  200: jsonResponse
func RegisterUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	customer := model.User{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&customer); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	// Get new customer id - database primary key
	var lastCustomerID model.User
	db.Last(&lastCustomerID)
	customer.ID = lastCustomerID.ID + 1
	// Get the current time - as date created
	current := time.Now()
	// timeNow := current.Format("2006-01-02 15:04:05")
	customer.DateCreated = current

	referralCode := strings.Replace(customer.Name, " ", "", -1) + utils.GenerateRandomString(4)
	log.Println("Referral Code   >>>  ", referralCode)
	customer.ReferralCode = strings.ToLower(referralCode)

	// Password  - Encrypt
	log.Println("customer.Password >>> ", customer.Password)
	// Encrypt the users pwd before saving to db
	encrypted, err := utils.EncryptString(customer.Password, passphrase)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("encrypted >>> ", encrypted)
		customer.Password = encrypted
	}

	decrypted, err := utils.DecryptString(customer.Password, passphrase)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("decrypted >>> ", decrypted)
		// w.WriteHeader(http.StatusUnauthorized)
	}

	log.Println("Event Name     >>>  ", customer.Name)
	customer.Status = 1 // Active

	response := model.JSONResponse{}

	if err := db.Save(&customer).Error; err != nil {
		// respondError(w, http.StatusInternalServerError, err.Error())
		response.Status = "01"
		response.Description = "Error creating customer." + err.Error()
		respondJSON(w, http.StatusCreated, response)

	} else {

		// No email sending - sent via seperate user creation request
		// Create activity log
		activity := model.Activity{}
		activity.ActivityType = "CUSTOMER_SIGN_UP"
		activity.Description = "User Sign up [" + customer.Name + "], User ID : " + utils.ConvertIntToString(customer.ID)
		activity.ResourceID = customer.ID
		activity.UserID = customer.ID
		CreateActivity(db, activity)

		// Send email to Gidemn on new event registration
		// CreateMemberRegistrationEmail(db, event)

		// Welcome the new event to BidsMwenda
		// NewMemberRegistrationEmail(db, event)

		response.Status = "00"
		response.Description = "User registerd successfully."
		respondJSON(w, http.StatusCreated, response)

	}

}

// UserLogin - UserLogin
// swagger:route GET /users Users users
// User login validation - Email and password need to be passed through Basic Authentication.
// JSON Response is provided with respective success or fail details.
// responses:
//  200: userLoginResponse
func UserLogin(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	basicAuth := r.Header.Get("Authorization")
	log.Println("Authenticating user [basicAuth] >>> ", basicAuth)

	s := strings.Split(basicAuth, " ")
	value := s[1]
	// log.Println(key, value)

	details := utils.Base64DecodeString(value)
	// log.Println("Header details >>> ", details)

	userDetails := strings.Split(details, ":")
	email, password := userDetails[0], userDetails[1]
	// log.Println(email, password)

	validateUser := model.User{}
	_ = json.NewDecoder(r.Body).Decode(&validateUser)
	log.Println("Authenticating user >>> ", email)
	// log.Println("Authenticating pwd  >>> ", validateUser.Password)

	customerValidation := model.User{}
	db.Where("email = ? ", email).Find(&customerValidation).First(&customerValidation)

	if customerValidation.ID != 0 {

		// log.Println("User ID >>> ", customerValidation.ID)
		decrypted, err := utils.DecryptString(customerValidation.Password, passphrase)
		if err != nil {
			log.Println(err)
		} else {
			// log.Println("decrypted >>> ", decrypted)
			// w.WriteHeader(http.StatusUnauthorized)
		}

		// log.Println("Decrypted Pwd >>> ", decrypted)
		if password == decrypted {
			log.Println("Valid User Login. Generating token...")
			// w.Write([]byte("OK"))
			customerLogin := GetCustomerDetails(db, customerValidation.ID)
			utils.CreateTokenEndpointCustom(w, customerLogin)
			// w.WriteHeader(http.StatusOk)
		} else {
			log.Println("Invalid User!")
			w.WriteHeader(http.StatusUnauthorized)
		}

	} else {
		validateUser.Status = 0
		log.Println("Invalid User!")
		// json.NewEncoder(w).Encode(validate_user)
		w.WriteHeader(http.StatusUnauthorized)
	}
}

// GetCustomerDetails - GetCustomerDetails
func GetCustomerDetails(db *gorm.DB, customerID int) *model.User {
	customer := model.User{}
	db.Where("id = ?", customerID).Find(&customer).First(&customer)
	return &customer
}
