package handler

import (
	"net/http"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// GetAllActivityCount - GetAllActivityCount
func GetAllActivityCount(db *gorm.DB) float64 {
	var result model.Result
	// # 0 Male # 1 Female
	db.Raw("select count(*) as total from activity ").Scan(&result)
	return result.Total
}

// GetAllActivity - GetAllActivity
func GetAllActivity(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	allActivity := []model.Activity{}
	db.Order("Id DESC").Find(&allActivity)

	activityReport := []model.ActivityReport{}

	for _, activity := range allActivity {

		report := model.ActivityReport{}
		report.Activity = activity
		report.ResourceID = activity.ResourceID
		report.ActionedBy = activity.UserID // *GetMemberDetails(db, activity.UserID)

		switch activity.ActivityType {
		case "USER_UPDATE":
			// user := GetMemberDetails(db, activity.ResourceID)
			// report.Resource = user
		case "USER_CREATION":
			// user := GetMemberDetails(db, activity.ResourceID)
			// report.Resource = user
		case "USER_PASSWORD_RESET":
			// user := GetMemberDetails(db, activity.ResourceID)
			// report.Resource = user
		}

		activityReport = append(activityReport, report)
	}

	respondJSON(w, http.StatusOK, activityReport)
}

// GetAllOrganizationActivity - GetAllOrganizationActivity
func GetAllOrganizationActivity(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	orgIDStr := vars["id"]
	orgID := utils.ConvertStringToInt(orgIDStr)

	allActivity := []model.Activity{}
	db.Where("user_id in (select id from users where organization_id = ?) ", orgID).Order("ID DESC").Find(&allActivity)

	activityReport := []model.ActivityReport{}

	for _, activity := range allActivity {

		report := model.ActivityReport{}
		report.Activity = activity
		report.ResourceID = activity.ResourceID
		report.ActionedBy = activity.UserID // *GetMemberDetails(db, activity.UserID)

		switch activity.ActivityType {
		case "USER_UPDATE":
			// user := GetMemberDetails(db, activity.ResourceID)
			// report.Resource = user
		case "USER_CREATION":
			// user := GetMemberDetails(db, activity.ResourceID)
			// report.Resource = user
		case "USER_PASSWORD_RESET":
			// user := GetMemberDetails(db, activity.ResourceID)
			// report.Resource = user
		}

		activityReport = append(activityReport, report)
	}

	respondJSON(w, http.StatusOK, activityReport)
}

// GetSingleActivity - GetSingleActivity
func GetSingleActivity(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	activityIDString := vars["id"]
	activityID := utils.ConvertStringToInt(activityIDString)

	activity := GetActivityDetails(db, activityID)
	report := model.ActivityReport{}
	report.Activity = *activity
	report.ResourceID = activity.ResourceID
	report.ActionedBy = activity.UserID // *GetMemberDetails(db, activity.UserID)

	switch activity.ActivityType {
	case "USER_UPDATE":
		// user := GetMemberDetails(db, activity.ResourceID)
		// report.Resource = user
	case "USER_CREATION":
		// user := GetMemberDetails(db, activity.ResourceID)
		// report.Resource = user
	case "USER_PASSWORD_RESET":
		// user := GetMemberDetails(db, activity.ResourceID)
		// report.Resource = user

	}

	respondJSON(w, http.StatusOK, report)
}

// CreateActivity - CreateActivity
func CreateActivity(db *gorm.DB, activity model.Activity) int {

	// Get new org id - database primary key
	var lastActivity model.Activity
	db.Last(&lastActivity)
	activity.ID = lastActivity.ID + 1

	// Get the current time - as date created
	current := time.Now()
	timeNow := current.Format("2006-01-02 15:04:05")
	activity.CreatedOn = timeNow

	if err := db.Save(&activity).Error; err != nil {
		return 0
	} else {
		return activity.ID
	}
}

// GetActivityDetails gets a user instance if exists, or respond the 404 error otherwise
func GetActivityDetails(db *gorm.DB, activityID int) *model.Activity {
	activity := model.Activity{}
	if err := db.First(&activity, model.Activity{ID: activityID}).Error; err != nil {
		return &activity
	}
	return &activity
}
