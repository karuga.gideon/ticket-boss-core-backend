package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// GetAllTicketTypes - GetAllTicketTypes
func GetAllTicketTypes(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	ticketTypes := []model.TicketType{}
	db.Order("ID ASC").Find(&ticketTypes)
	respondJSON(w, http.StatusOK, ticketTypes)
}

// CreateTicketType - Create a new ticket type for an event.
// swagger:route POST /events/{id}/tickettypes Events ticketTypeRequest
// Create a new ticket type for an event.
// JSON Response is provided with respective success or fail details.
// responses:
//  200: jsonResponse
func CreateTicketType(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	eventIDStr := vars["id"]
	eventID := utils.ConvertStringToInt(eventIDStr)

	event := GetEventDetails(db, eventID)
	response := model.JSONResponse{}

	if event.ID > 0 {

		log.Println("Creating ticket type for event >>>> ", event.Name)

		ticketType := model.TicketType{}
		// Get ticket type details from posted data
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&ticketType); err != nil {
			respondError(w, http.StatusBadRequest, err.Error())
			return
		}
		defer r.Body.Close()

		var lastTicketType model.TicketType
		db.Last(&lastTicketType)
		ticketType.ID = lastTicketType.ID + 1
		ticketType.EventID = event.ID
		ticketType.DateCreated = time.Now()
		ticketType.Status = 1

		if err := db.Save(&ticketType).Error; err != nil {
			response.Status = "01"
			response.Description = "Error creating Ticket type." + err.Error()
		} else {
			response.Status = "00"
			response.Description = "Ticket type created successfully."
		}

		respondJSON(w, http.StatusOK, response)

	} else {
		response.Status = "01"
		response.Description = "Event not found!"
		respondJSON(w, http.StatusOK, response)
	}

}

// GetEventTicketTypes - GetEventTicketTypes
func GetEventTicketTypes(db *gorm.DB, eventID int) *[]model.TicketType {
	eventTicketTypes := []model.TicketType{}
	log.Println("Getting eventTicketTypes by eventID >>> ", eventID)
	db.Where("event_id = ?", eventID).Find(&eventTicketTypes).Order("ID ASC")
	return &eventTicketTypes
}

// GetTicketTypeDetails - GetTicketTypeDetails
func GetTicketTypeDetails(db *gorm.DB, ticketTypeID int) *model.TicketType {
	ticketType := model.TicketType{}
	db.Where("id = ?", ticketTypeID).Find(&ticketType).First(&ticketType)
	return &ticketType
}
