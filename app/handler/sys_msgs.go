package handler

import (
	"log"
	"net/http"
	"strings"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"ticket-boss/config"
	"time"

	"github.com/jinzhu/gorm"
)

// GetAllSysMsgs - GetAllSysMsgs
func GetAllSysMsgs(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	sysMsgs := []model.SysMsg{}
	db.Order("Id DESC").Find(&sysMsgs)
	respondJSON(w, http.StatusOK, sysMsgs)
}

// GetSysMsg gets a user instance if exists, or respond the 404 error otherwise
func GetSysMsg(db *gorm.DB, sysMsgID int) *model.SysMsg {
	sysMsg := model.SysMsg{}
	if err := db.First(&sysMsg, model.SysMsg{ID: sysMsgID}).Error; err != nil {
		return &sysMsg
	}
	return &sysMsg
}

// CheckForSysMsgsToSend - CheckForSysMsgsToSend
func CheckForSysMsgsToSend(config config.Config, db *gorm.DB) {

	for i := 0; ; i++ {

		// log.Println("Checking for unsent messages to send...")

		// Get all pending sys msgs - check status
		unsentMessages := []model.SysMsg{}
		db.Order("Id DESC").Where("status = 0 ").Limit(100).Find(&unsentMessages)

		for _, sysMsg := range unsentMessages {
			// Updated accordingly whether successful or failed
			response := model.SysMsg{}

			if strings.ToUpper(sysMsg.MsgType) == "EMAIL" {
				log.Println("About to Send Email ID >>> ", sysMsg.ID)
				response = utils.SendCustomEmail(sysMsg)
				log.Println("Send Email Response    >>> ", response.Status)
			} else {
				log.Println("About to Send SMS ID >>> ", sysMsg.ID)
				// response = utils.SendSMSNexmo(sysMsg)
				log.Println("Send SMS Response    >>> ", response.Status)
			}

			if response.Status == 2 {
				db.Model(&sysMsg).UpdateColumns(model.SysMsg{Status: 2, DateSent: time.Now(), TrxID: response.TrxID})
			} else if response.Status == 3 {
				db.Model(&sysMsg).UpdateColumns(model.SysMsg{Status: 3, DateSent: time.Now(), TrxID: response.TrxID})
			}

		}

		utils.DelaySeconds(2) // Check every 5 seconds
	}

}
