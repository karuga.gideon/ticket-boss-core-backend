package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"ticket-boss/app/model"
	"ticket-boss/app/swagger"
	"ticket-boss/app/utils"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// GetAllEvents - GetAllEvents
// swagger:route GET /events Events events
// Get All Events.
// Get All available events.
// responses:
//  200: allEventsResponse
func GetAllEvents(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	events := []model.Event{}
	db.Order("ID DESC").Find(&events)

	swaggerEventsResponse := swagger.EventsResponse{}
	swaggerEventsResponse.Data = events

	respondJSON(w, http.StatusOK, events)
}

// GetTotalEventsCount - GetTotalEventsCount
func GetTotalEventsCount(db *gorm.DB) float64 {
	var result model.Result
	// # 0 Male # 1 Female
	db.Raw("select count(*) as total from events").Scan(&result)
	return result.Total
}

// CreateEvent - Event Creation
// swagger:route POST /events Events eventRequest
// Creates a new Event.
// JSON Response is provided with respective success or fail details for event creation.
// responses:
//  200: jsonResponse
func CreateEvent(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	event := model.Event{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&event); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	// Get new event id - database primary key
	var lastEventID model.Event
	db.Last(&lastEventID)
	event.ID = lastEventID.ID + 1

	// Get the current time - as date created
	current := time.Now()
	// timeNow := current.Format("2006-01-02 15:04:05")
	event.DateCreated = current
	log.Println("Event Name     >>>  ", event.Name)
	event.Status = 1 // Active

	response := model.JSONResponse{}

	if err := db.Save(&event).Error; err != nil {
		// respondError(w, http.StatusInternalServerError, err.Error())
		response.Status = "01"
		response.Description = "Error creating event." + err.Error()
		respondJSON(w, http.StatusCreated, response)

	} else {

		// No email sending - sent via seperate user creation request
		// Create activity log
		activity := model.Activity{}
		activity.ActivityType = "EVENT_CREATION"
		activity.Description = "Event Creation [" + event.Name + "], Event ID : " + utils.ConvertIntToString(event.ID)
		activity.ResourceID = event.ID
		activity.UserID = event.ID
		CreateActivity(db, activity)

		// Send email to Gidemn on new event registration
		// CreateMemberRegistrationEmail(db, event)

		// Welcome the new event to BidsMwenda
		// NewMemberRegistrationEmail(db, event)

		response.Status = "00"
		response.Description = "Event created successfully."
		respondJSON(w, http.StatusCreated, response)

	}

}

// GetSingleEvent - GetSingleEvent Details
// swagger:route GET /events/{id} Events singleEventRequest
// Gets a single event details.
// JSON Response is provided with respective success or fail details for event.
// responses:
//  200: singleEventResponse
func GetSingleEvent(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	eventIDStr := vars["id"]
	eventID := utils.ConvertStringToInt(eventIDStr)

	event := *GetEventDetails(db, eventID)
	response := model.EventDetailsResult{}

	response.Event = event
	response.TicketTypes = *GetEventTicketTypes(db, eventID)

	respondJSON(w, http.StatusCreated, response)
}

// GetEventDetails - GetEventDetails
func GetEventDetails(db *gorm.DB, eventID int) *model.Event {
	event := model.Event{}
	db.Where("id = ?", eventID).Find(&event).First(&event)
	return &event
}
