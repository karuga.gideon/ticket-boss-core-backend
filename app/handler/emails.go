package handler

import (
	"log"
	"net/http"
	"ticket-boss/app/model"
	"ticket-boss/app/utils"
	"time"

	"github.com/jinzhu/gorm"
)

// TestEmail - TestEmail
func TestEmail(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	// utils.SendEmailTest()
	// utils.SendEmailTwo()

	msg := model.SysMsg{}
	msg.MsgType = "Email"
	msg.Subject = "Test Email"
	msg.Recipient = "karuga.gideon@gmail.com"
	msg.Message = "BidsMwenda test email."
	msg.Status = 0

	msgResponse := utils.SendCustomEmail(msg)
	// utils.SendEmailThree()
	log.Println("Send Email Response Status  >>>> ", msgResponse.Status)

	response := model.JSONResponse{}
	response.Status = "00"
	response.Description = "Email Sent."
	respondJSON(w, http.StatusOK, response)

}

// CreateBidEmail - CreateBidEmail
func CreateBidEmail(db *gorm.DB, receipient string, transaction model.Transaction, memberName string) {

	msg := model.SysMsg{}

	var lastMsg model.SysMsg
	db.Last(&lastMsg)
	msg.ID = lastMsg.ID + 1

	msg.DateCreated = time.Now()
	msg.DateSent = time.Now()

	msg.MsgType = "Email"
	msg.Subject = "Bid Placed"
	msg.Recipient = receipient

	amount := utils.FloatToString(transaction.Amount)
	message := "Dear <b>" + memberName + "</b>,<br/><br/> Bid of amount KSh " + amount + " successfully placed."
	message += "<br/><br/><b>Current Statistics</b><br/><br/>"
	message += "Total Events : " + utils.FloatToString(GetTotalEventsCount(db)) + "<br/>"
	message += "Total registered members : " + utils.FloatToString(GetTotalRegisteredCustomersCount(db)) + "<br/>"
	message += "Total Transactions count : " + utils.FloatToString(GetTotalTransactionsCount(db)) + "<br/>"
	message += "Total Transactions sum : " + utils.FloatToString(GetTotalTransactionsSum(db)) + "<br/>"
	message += "<br/><br/>Br,<br/>Bids Mwenda."
	msg.Message = message
	msg.Status = 0

	// msgResponse := utils.SendCustomEmail(msg)
	// utils.SendEmailThree()
	// log.Println("Send Email Response Status  >>>> ", msgResponse.Status)

	if err := db.Save(&msg).Error; err != nil {
		utils.CheckError(err, "CreateMemberRegistrationEmail")
	} else {
		log.Println("User created successfully.")
	}
}

// CreateStatsEmail - CreateStatsEmail
func CreateStatsEmail(db *gorm.DB, receipient string) {

	msg := model.SysMsg{}

	var lastMsg model.SysMsg
	db.Last(&lastMsg)
	msg.ID = lastMsg.ID + 1

	msg.DateCreated = time.Now()
	msg.DateSent = time.Now()

	msg.MsgType = "Email"
	msg.Subject = "Ticket Boss : Hourly Stats"
	msg.Recipient = receipient

	message := "Dear <b>Gideon</b>,<br/><br/> The following are the houry stats."
	message += "<br/><br/><b>Current Statistics</b><br/><br/>"
	message += "Total Events : " + utils.FloatToString(GetTotalEventsCount(db)) + "<br/>"
	message += "Total registered members : " + utils.FloatToString(GetTotalRegisteredCustomersCount(db)) + "<br/>"
	message += "Total Transactions count : " + utils.FloatToString(GetTotalTransactionsCount(db)) + "<br/>"
	message += "Total Transactions sum : " + utils.FloatToString(GetTotalTransactionsSum(db)) + "<br/>"
	message += "<br/><br/>Br,<br/>Bids Mwenda."
	msg.Message = message
	msg.Status = 0

	// msgResponse := utils.SendCustomEmail(msg)
	// utils.SendEmailThree()
	// log.Println("Send Email Response Status  >>>> ", msgResponse.Status)

	if err := db.Save(&msg).Error; err != nil {
		utils.CheckError(err, "CreateMemberRegistrationEmail")
	} else {
		log.Println("User created successfully.")
	}
}

// CreateMemberRegistrationEmail - CreateMemberRegistrationEmail
func CreateMemberRegistrationEmail(db *gorm.DB, member model.User) {

	var lastMsg model.SysMsg
	db.Last(&lastMsg)

	msg := model.SysMsg{}
	msg.ID = lastMsg.ID + 1
	msg.DateCreated = time.Now()
	msg.DateSent = time.Now()

	msg.MsgType = "EMAIL"
	msg.Subject = "New User Registration : " + member.Name
	msg.Recipient = "karuga.gideon@gmail.com"
	message := "Dear <b>Gideon</b>,<br/><br/> There has been a new member registration with the following details; <br/><br/>"
	message += "<b>Name   :</b> " + member.Name + "<br/>"
	message += "<b>Phone  :</b> " + member.Phone + "<br/>"
	message += "<b>Email  :</b> " + member.Email + "<br/>"
	message += "<b>RefCd  :</b> " + member.ReferralCode + "<br/>"
	message += "<br/>Br,<br/>BidsMwenda." + "<br/>"
	msg.Message = message
	msg.Status = 0

	if err := db.Save(&msg).Error; err != nil {
		utils.CheckError(err, "CreateMemberRegistrationEmail")
	} else {
		log.Println("User created successfully.")
	}

	// msgResponse := utils.SendCustomEmail(msg)
	// utils.SendEmailThree()
	// log.Println("Send Email Response Status  >>>> ", msgResponse.Status)
}

// NewMemberRegistrationEmail - NewMemberRegistrationEmail
func NewMemberRegistrationEmail(db *gorm.DB, member model.User) {

	var lastMsg model.SysMsg
	db.Last(&lastMsg)

	msg := model.SysMsg{}
	msg.ID = lastMsg.ID + 1
	msg.DateCreated = time.Now()
	msg.DateSent = time.Now()

	referralLink := "http://www.bidsmwenda.com/sign-up/" + member.ReferralCode

	msg.MsgType = "EMAIL"
	msg.Subject = "BidsMwenda : User Registration"
	msg.Recipient = member.Email
	message := "Dear <b>" + member.Name + "</b>,<br/><br/>"
	message += "Welcome to Bids Mwenda platform, your registration was successful. <br/>"
	message += "Happy Winnings!!<br/><br/>"
	message += "Below is your referral link which you can use to invite friends; <br/><br/>"
	message += "<a href=\"" + referralLink + "\">" + referralLink + "</a><br/><br/>"
	message += "Regards,<br/>BidsMwenda." + "<br/>"
	msg.Message = message
	msg.Status = 0

	if err := db.Save(&msg).Error; err != nil {
		utils.CheckError(err, "NewMemberRegistrationEmail")
	} else {
		log.Println("User email created successfully.")
	}

	// msgResponse := utils.SendCustomEmail(msg)
	// utils.SendEmailThree()
	// log.Println("Send Email Response Status  >>>> ", msgResponse.Status)
}
