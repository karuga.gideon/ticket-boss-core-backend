

# SWAGGER UI Documentation generation 


swagger generate spec -o ./swagger.json --scan-models

swagger generate server -f ./swagger.json -A ticketboss --principal ticketboss

swagger serve -F=swagger swagger.json



# After generation, copy the generated swagger.json file into swaggerui folder 



