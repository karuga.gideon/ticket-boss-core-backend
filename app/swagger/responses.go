package swagger

import "ticket-boss/app/model"

// Success response
// swagger:response ok
type swaggerSuccessResponse struct {
	// in:body
	Body struct {
		// HTTP status code 200 - OK
		Code int `json:"code"`
	}
}

// Error Forbidden
// swagger:response forbidden
type swaggerErrorForbidden struct {
	// in:body
	Body struct {
		// HTTP status code 403 -  Forbidden
		Code int `json:"code"`
		// Detailed error message
		Message string `json:"message"`
	}
}

// EventsResponse001 : HTTP status code 200 and an array of repository models in data
// swagger:response allEventsResponse001
type EventsResponse001 struct {
	// in:body
	Body struct {
		// HTTP status code 200 - Status OK
		Code int `json:"code"`
		// Array of repository models
		Data []model.Event `json:"data"`
	}
}

// EventsResponse : HTTP status code 200 and an array of event models in data
// swagger:response allEventsResponse
type EventsResponse struct {
	// in:body
	Data []model.Event
}

// SingleEventResponse : HTTP status code 200 and an array of event models in data
// swagger:response sigleEventResponse
type SingleEventResponse struct {
	// in:body
	Data model.EventDetailsResult
}

// UsersResponse : HTTP status code 200 and an array of user models in data
// swagger:response allUsersResponse
type UsersResponse struct {
	// in:body
	Data []model.User
}

// UserLoginResponse : HTTP status code 200 and an array of user models in data
// swagger:response userLoginResponse
type UserLoginResponse struct {
	// in:body
	Data model.JwtToken
}

// SwagerEventRequest table / model
// swagger:response eventResponse
type swaggerCreateEventResponse struct {
	// in:body
	Body model.Event
}

// EventDetailsResult - Return Event details with respective ticket types
// swagger:response singleEventResponse
type EventDetailsResult struct {

	// in:body
	Body struct {
		Event       model.Event        `json:"event"`
		TicketTypes []model.TicketType `json:"ticket_types"`
	}
}
