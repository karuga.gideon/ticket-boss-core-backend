package swagger

// SwagerEventRequest table / model
// swagger:parameters eventRequest
type swaggerCreateEventRequest struct {
	// in:body
	Body struct {
		Name               string `json:"name"`
		Description        string `json:"description"`
		EventStartDate     string `json:"event_start_date"`
		EventEndDate       string `json:"event_end_date"`
		ContactPersonName  string `json:"contact_person_name"`
		ContactPersonPhone string `json:"contact_person_phone"`
		ContactPersonEmail string `json:"contact_person_email"`
		BannerURLImg1      string `json:"banner_url_img1"`
		BannerURLImg2      string `json:"banner_url_img2"`
	}
}

// SwagerUserCreationRequest table / model
// swagger:parameters userRequest
type swaggerCreateUserRequest struct {
	// in:body
	Body struct {
		Name     string `json:"name"`
		Phone    string `json:"phone"`
		Email    string `json:"email"`
		Password string `json:"password"`
		Type     int    `json:"type"` // 1 -  Admin, 2 - Customer
		ImgURL   string `json:"img_url"`
	}
}

// SwagerUserCreationRequest table / model
// swagger:parameters ticketTypeRequest
type swaggerCreateTicketTypeRequest struct {

	//in:path
	ID int `json:"id"`

	// in:body
	Body struct {
		Name        string  `json:"name"`
		Description string  `json:"description"`
		Amount      float64 `json:"amount"`
	}
}

// SwagerUserCreationRequest table / model
// swagger:parameters singleEventRequest
type swaggerSingleEventRequest struct {

	//in:path
	ID int `json:"id"`
}
