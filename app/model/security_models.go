package model

// JwtToken - returned after user login
type JwtToken struct {
	Status      string `json:"status"`
	Token       string `json:"token"`
	UserDetails User   `json:"user_details"`
}
