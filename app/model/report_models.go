package model

// EventDetailsResult - Return Event details with respective ticket types
type EventDetailsResult struct {
	Event       Event        `json:"event"`
	TicketTypes []TicketType `json:"ticket_types"`
}

// StringResult - db result
// swagger:response stringResult
type StringResult struct {
	Name string `json:"name"`
}

// JSONResponse struct
// swagger:response jsonResponse
type JSONResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}

// Result - db result
// swagger:response result
type Result struct {
	Total float64 `json:"total"`
}

// ChangePassword - ChangePassword
// swagger:response changePassword
type ChangePassword struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

// ActivityReport table struct
type ActivityReport struct {
	Activity   Activity    `json:"activity"`
	ResourceID int         `json:"resource_id"`
	Resource   interface{} `json:"resource_details"`
	ActionedBy int         `json:"actioned_by"`
}

// StatisticsReport struct
// swagger:response statisticsReport
type StatisticsReport struct {
	TotalEvents             float64 `json:"total_events"`
	TotalMembers            float64 `json:"total_members"`
	TotalTransactionsCount  float64 `json:"total_transactions_count"`
	TotalTransactionsAmount float64 `json:"total_transactions_amount"`
}
