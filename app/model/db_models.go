package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Event table / model
// swagger:response event
type Event struct {
	ID                 int       `json:"id"`
	Name               string    `json:"name"`
	Description        string    `json:"description"`
	DateCreated        time.Time `json:"date_created"`
	EventStartDate     string    `json:"event_start_date"`
	EventEndDate       string    `json:"event_end_date"`
	Status             int       `json:"status"` // 0 - Pending Activation, 1 - Active, - 2. Event ended, 3. - Event Cancelled
	ContactPersonName  string    `json:"contact_person_name"`
	ContactPersonPhone string    `json:"contact_person_phone"`
	ContactPersonEmail string    `json:"contact_person_email"`
	BannerURLImg1      string    `json:"banner_url_img1"`
	BannerURLImg2      string    `json:"banner_url_img2"`
}

// TicketType table / model
// swagger:response ticketType
type TicketType struct {
	ID          int       `json:"id"`
	DateCreated time.Time `json:"date_created"`
	EventID     int       `json:"event_id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Amount      float64   `json:"amount"`
	Status      int       `json:"status"` // 0 - Inactive , 1 - Active
}

// User table / model
// swagger:response user
type User struct {
	ID           int       `json:"id"`
	Name         string    `json:"name"`
	Phone        string    `json:"phone"`
	Email        string    `json:"email"`
	Password     string    `json:"password"`
	DateCreated  time.Time `json:"date_created"`
	Type         int       `json:"type"`   // 1 -  Admin, 2 - Customer
	Status       int       `json:"status"` // 0 - Inactive, 1 - Active
	ReferralCode string    `json:"referral_code"`
	ImgURL       string    `json:"img_url"`
}

// Transaction table / model
// swagger:response transaction
type Transaction struct {
	ID                int     `json:"id"`
	TrxID             string  `json:"trx_id"`
	CustomerID        int     `json:"customer_id"`
	EventID           int     `json:"event_id"`
	TicketTypeID      int     `json:"ticket_type_id"`
	Amount            float64 `json:"amount"`
	Status            int     `json:"status"` // 0 - Pending payment / processing, 1- Processing , 2 - Paid, 3 - Failed
	StatusDescription string  `json:"status_description"`
	ThirdPartyTrxID   string  `json:"third_party_trx_id"`
}

// Activity table struct
// swagger:response activity
type Activity struct {
	ID           int    `json:"id"`
	UserID       int    `json:"user_id"`
	ActivityType string `json:"activity_type"`
	Description  string `json:"description"`
	SourceIP     string `json:"source_ip"`
	ResourceID   int    `json:"resource_id"`
	CreatedOn    string `json:"created_on"`
}

// SysMsg struct
// swagger:response sysmsg
type SysMsg struct {
	ID               int       `json:"id"`
	DateCreated      time.Time `json:"date_created"`
	MsgType          string    `json:"msg_type"` // EMAIL or SMS
	Recipient        string    `json:"recipient"`
	Subject          string    `json:"subject"`
	Message          string    `json:"message"`
	NotificationType string    `json:"notification_type"`
	Status           int       `json:"status"` // 0 - New, 1 - Processing, 2 - Sent, 3 - Failed
	SgMemberID       int       `json:"sg_member_id"`
	DateSent         time.Time `json:"date_sent"`
	TrxID            string    `json:"trx_id"`
}

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&Event{}, &TicketType{}, &User{}, &Transaction{}, &Activity{}, &SysMsg{})
	// db.Model(&User{}).AddForeignKey("org_id", "organizations(id)", "CASCADE", "CASCADE")
	return db
}
