package model

// NexmoMessage struct
type NexmoMessage struct {
	MessageCount string    `json:"message-count"`
	Messages     []Message `json:"messages"`
}

// Message table / model
type Message struct {
	To               string `json:"to"`
	MessageID        string `json:"message-id"`
	Status           string `json:"status"`
	RemainingBalance string `json:"remaining-balance"`
	MessagePrice     string `json:"message-price"`
	Network          string `json:"network"`
}
