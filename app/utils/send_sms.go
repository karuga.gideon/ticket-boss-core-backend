package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"ticket-boss/app/model"
)

// SendBidSMS - SendBidSMS
func SendBidSMS(msisdn string) {

	// message :=
	url := "http://payments.strutstechnology.co.ke/pesapal/at_sms.php?msisdn=" + msisdn + "&message=Bid%20Successfully%20placed.%20You%20will%20be%20notified%20if%20you%20are%20the%20lucky%20winner.&key=5ab3c9c9e76944bcb05b0a2"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "3d3d26a3-fa1b-432c-af91-bfdbe8f0f40b")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	log.Println(res)
	log.Println(string(body))
}

// SendSMSNexmo - SendSMSNexmo
func SendSMSNexmo(sysMsg model.SysMsg) model.SysMsg {

	// url := "https://rest.nexmo.com/sms/json"

	recipient := sysMsg.Recipient
	message := sysMsg.Message

	log.Println("Sending SMS, Recipient >>> ", recipient)
	log.Println("Sending SMS, Message   >>> ", message)

	message = URLEncodeString(message)
	log.Println("Sending SMS, Message [1]  >>> ", message)

	// url := "https://rest.nexmo.com/sms/json?from=Save&text=Hello%20from%20Nexmo&to=250727116186&api_key=de087b65&api_secret=9c629de08d20b047&undefined="

	url := "https://rest.nexmo.com/sms/json?from=Save&text=" + message + "&to=" + recipient + "&api_key=de087b65&api_secret=9c629de08d20b047&undefined="

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "7c08fd2f-01ae-45ec-b348-b0e214266320")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

	fmt.Println(res)
	fmt.Println(string(body))

	// fmt.Println(res)
	responseString := string(body)
	fmt.Println(responseString)
	responseString = strings.ToUpper(responseString)
	fmt.Println(responseString)

	var nexmoMessage model.NexmoMessage
	json.Unmarshal([]byte(string(body)), &nexmoMessage)

	log.Println("MessageCount          >>> ", nexmoMessage.MessageCount)
	log.Println("Messages MessageID    >>> ", nexmoMessage.Messages[0].MessageID)

	if len(nexmoMessage.MessageCount) > 0 {
		sysMsg.Status = 2
		sysMsg.TrxID = nexmoMessage.Messages[0].MessageID
	}

	return sysMsg
}

// SendSMSMTN - SendSMSMTN
func SendSMSMTN(sysMsg model.SysMsg) model.SysMsg {

	// http://bulksms.mtn.co.rw:3060/send?username=pindo&password=3PiNBq&to=250781534563&content=Save SMS Test&from=Save
	recipient := sysMsg.Recipient
	message := sysMsg.Message

	log.Println("Sending SMS, Recipient >>> ", recipient)
	log.Println("Sending SMS, Message   >>> ", message)

	message = URLEncodeString(message)
	log.Println("Sending SMS, Message [1]  >>> ", message)

	// url := "http://bulksms.mtn.co.rw:3060/send?username=pindo&password=3PiNBq&to=250781534563&content=Twishimiye%20kubamenyesha%20ko%20itsinda%20SG%20Mania%20ryahawe%20ishimwe%20rya%20%20Rwf%201%20na%20Gidemn%20Intl.%20Mukomeze%20muryoherwe%20na%20services%20za%20SAVE.%20Murakoze.&from=Save"
	url := "http://bulksms.mtn.co.rw:3060/send?username=pindo&password=3PiNBq&to=" + recipient + "&content=" + message + "&from=Save"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "ea6cb2d1-7e39-4c28-8d17-997865c06bb3")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	// fmt.Println(res)
	responseString := string(body)
	fmt.Println(responseString)
	responseString = strings.ToUpper(responseString)
	fmt.Println(responseString)

	success := strings.Contains(responseString, "SUCCESS") // true
	sysMsg.TrxID = responseString

	if success {
		sysMsg.Status = 2
	} else {
		sysMsg.Status = 3
	}

	return sysMsg

}
