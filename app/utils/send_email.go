package utils

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/mail"
	"net/smtp"
	"ticket-boss/app/model"
	"time"

	"github.com/jinzhu/gorm"
)

// SendEmailTest - SendEmailTest  - For testing purposes only..
func SendEmailTest() {
	message := model.SysMsg{}
	message.Subject = "Test"
	message.Message = "Test Message - ONE."
	message.Recipient = "karuga.gideon@gmail.com"
	SendEmail(message)
}

// SendEmailTwo - SendEmailTwo  - For testing purposes only..
func SendEmailTwo() {
	from := "alerts@bidsmwenda.com"
	auth := smtp.PlainAuth("Test Email", from, "MwendaAlerts2050#", "smtp.zoho.com")
	// "Subject: Test Email Subject",       // Subject
	err := smtp.SendMail(
		"smtp.zoho.com:465", // server address
		auth,                // authentication
		from,                // sender's address
		[]string{"karuga.gideon@gmail.com"},                                             // recipients' address
		[]byte("Subject: Test Subject 2\r\n\r\nThis is the body - Test Message - TWO."), // Subject and Body
	)
	if err != nil {
		log.Print(err)
	}
}

// SendEmailThree - SendEmailThree
func SendEmailThree() {
	from := mail.Address{"", "alerts@bidsmwenda.com"}
	to := mail.Address{"", "karuga.gideon@gmail.com"}
	subj := "This is the email subject"
	body := "This is an example body.\n With two lines."

	// Setup headers
	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subj

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	// Connect to the SMTP Server
	servername := "smtp.zoho.com:465"

	host, _, _ := net.SplitHostPort(servername)

	auth := smtp.PlainAuth("", "alerts@bidsmwenda.com", "MwendaAlerts2050#", host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	// Here is the key, you need to call tls.Dial instead of smtp.Dial
	// for smtp servers running on 465 that require an ssl connection
	// from the very beginning (no starttls)
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		log.Println(err)
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		log.Println(err)
	}

	// Auth
	if err = c.Auth(auth); err != nil {
		log.Println(err)
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		log.Println(err)
	}

	if err = c.Rcpt(to.Address); err != nil {
		log.Println(err)
	}

	// Data
	w, err := c.Data()
	if err != nil {
		log.Println(err)
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		log.Println(err)
	}

	err = w.Close()
	if err != nil {
		log.Println(err)
	}

	c.Quit()
}

// CreateNewSysMsgObject - CreateNewSysMsgObject
func CreateNewSysMsgObject(db *gorm.DB) model.SysMsg {

	newSysMsgObj := model.SysMsg{}

	var lastSysMsg model.SysMsg
	db.Last(&lastSysMsg)
	newMsgID := lastSysMsg.ID + 1

	newSysMsgObj.ID = newMsgID
	newSysMsgObj.MsgType = "EMAIL"
	newSysMsgObj.DateCreated = time.Now()
	newSysMsgObj.Status = 0
	return newSysMsgObj
}

// SendCustomEmailORIG - SendCustomEmailORIG
func SendCustomEmailORIG(message model.SysMsg) model.SysMsg {

	log.Println("About to send " + message.Subject + " email to >>> " + message.Recipient)
	from := "alerts@bidsmwenda.com"
	auth := smtp.PlainAuth(message.Subject, from, "MwendaAlerts2050#", "smtp.zoho.com")

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: " + message.Subject + "\r\n" + mime
	emailMessage := "<a href=\"https://getsave.io/\"><img src=\"http://web.getsave.io/assets/img/save_logo(colored).png\" " +
		"style=margin-left:-10px;margin-top:20px;height:40px /></a><br/><br/>"
	emailMessage += "<p style=\"font-family:Verdana,Geneva,sans-serif;margin-left:0;color:#424242\">" + message.Message + "</p>"
	emailMessage += "\r\n\r\n"
	// Email Footer
	emailMessage += "<div style=\"padding:20px; background-color: #fafafa; font-family:Verdana,Geneva,sans-serif;font-size:12px;margin-left:0;color:#424242;\">"
	emailMessage += "You're receiving this message because you are a user on SAVE. <br/>"
	emailMessage += "Send us feedback at info@getsave.io or call us on 7777 for local calls and +250 788-381-810 for international calls.<br/>"
	emailMessage += "Save is a product of <a href=\"https://exuus.com/\">Exuus Ltd</a>.<br/>"
	emailMessage += "Exuus is a limited corporation registered in Republic of Rwanda.<br/></div>"

	message.Status = 2
	err := smtp.SendMail(
		"smtp.zoho.com:465", // server address
		auth,                // authentication
		from,                // sender's address
		[]string{message.Recipient},                                                 // recipients' address
		[]byte(subject+"\r\n\r\n<html><body><p>"+emailMessage+"</p></body></html>"), // Subject and Body
	)
	if err != nil {
		log.Print(err)
		message.Status = 3
	}
	return message
}

// SendCustomEmail - SendCustomEmail
func SendCustomEmail(message model.SysMsg) model.SysMsg {

	log.Println("About to send " + message.Subject + " email to >>> " + message.Recipient)
	// from := "alerts@bidsmwenda.com"
	// auth := smtp.PlainAuth(message.Subject, from, "MwendaAlerts2050#", "smtp.zoho.com")

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: " + message.Subject + "\r\n" + mime
	emailMessage := ""
	// emailMessage := "<a href=\"https://getsave.io/\"><img src=\"http://web.getsave.io/assets/img/save_logo(colored).png\" " +
	// 	"style=margin-left:-10px;margin-top:20px;height:40px /></a><br/><br/>"
	emailMessage += "<p style=\"font-family:Verdana,Geneva,sans-serif;margin-left:0;color:#424242\">" + message.Message + "</p>"
	emailMessage += "\r\n\r\n"
	// Email Footer
	emailMessage += "<div style=\"padding:20px; background-color: #fafafa; font-family:Verdana,Geneva,sans-serif;font-size:12px;margin-left:0;color:#424242;\">"
	emailMessage += "<a href=\"https://BidsMwenda.com/\">BidsMwenda.com</a> | info@bidsmwenda.com | All rights reserved(c) <br/>"
	emailMessage += "<br/>"
	emailMessage += "</div>"

	message.Status = 2
	message.DateSent = time.Now()

	from := mail.Address{"", "alerts@bidsmwenda.com"}
	to := mail.Address{"", message.Recipient}
	subj := subject
	body := emailMessage

	// Setup headers
	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subj

	// Setup message
	messageText := ""
	for k, v := range headers {
		messageText += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	messageText += "\r\n" + body

	// Connect to the SMTP Server
	servername := "smtp.zoho.com:465"

	host, _, _ := net.SplitHostPort(servername)

	auth := smtp.PlainAuth("", "alerts@bidsmwenda.com", "MwendaAlerts2050#", host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	// Here is the key, you need to call tls.Dial instead of smtp.Dial
	// for smtp servers running on 465 that require an ssl connection
	// from the very beginning (no starttls)
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		message.Status = 3
		log.Println(err)
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		message.Status = 3
		log.Println(err)
	}

	// Auth
	if err = c.Auth(auth); err != nil {
		message.Status = 3
		log.Println(err)
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		message.Status = 3
		log.Println(err)
	}

	if err = c.Rcpt(to.Address); err != nil {
		message.Status = 3
		log.Println(err)
	}

	// Data
	w, err := c.Data()
	if err != nil {
		message.Status = 3
		log.Println(err)
	}

	_, err = w.Write([]byte(messageText))
	if err != nil {
		message.Status = 3
		log.Println(err)
	}

	err = w.Close()
	if err != nil {
		message.Status = 3
		log.Println(err)
	}

	c.Quit()
	log.Println("Send Email Response Status  >>>> ", message.Status)

	return message
}

// CreateErrorEmail - CreateErrorEmail
func CreateErrorEmail(errorMessage string) {
	msg := model.SysMsg{}
	msg.MsgType = "Email"
	msg.Subject = "ERROR!!! BidsMwenda"
	msg.Recipient = "karuga.gideon@gmail.com"
	msg.Message = "Dear <b>Gideon</b>,<br/><br/> The following error occurred on BidsMwenda.com<br/><br/><b>" + errorMessage + "<b/>"
	msg.Status = 0

	msgResponse := SendCustomEmail(msg)
	// utils.SendEmailThree()
	log.Println("Send Email Response Status  >>>> ", msgResponse.Status)
}

// SendEmail - Send email using golang / zoho - working Ok.
func SendEmail(message model.SysMsg) {
	log.Print("Sending email via zoho...")
	from := "alerts@bidsmwenda.com"
	pass := "MwendaAlerts2050#"
	to := message.Recipient
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: " + message.Subject + "\n"
	log.Print(subject)
	msg := []byte(subject + mime + "<html><body><p>" + message.Message + "</p></body></html>")
	// smtp.SendMail(server, auth, from, to, msg)
	err := smtp.SendMail("gmail.com:465",
		smtp.PlainAuth("", from, pass, "gmail.com"),
		from, []string{to}, msg)

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}
	log.Print("Email sent.")
}
