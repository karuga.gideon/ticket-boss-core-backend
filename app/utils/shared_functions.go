package utils

import (
	"crypto/aes"
	"crypto/cipher"
	cryptoRand "crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/url"
	"strconv"
	"strings"
	"time"

	raven "github.com/getsentry/raven-go"
)

// GetErrorCode -- Gets the error code  / message from MTN - Error Response
func GetErrorCode(errorString string) string {

	var errorCode = errorString
	errorStart := strings.LastIndex(errorCode, "errorcode")
	// log.Println("errorStart : %v\n", errorStart)
	errorCode = errorCode[errorStart:len(errorCode)]
	errorEnd := strings.Index(errorCode, ">")
	// log.Println("errorEnd : %v\n", errorEnd)

	errorCode = errorString[errorStart:(errorStart + errorEnd)]
	errorCode = strings.Replace(errorCode, "errorcode=", "", 1)
	errorCode = strings.Replace(errorCode, "/", "", 1)
	errorCode = strings.Replace(errorCode, "\"", "", 2)
	// log.Println("errorCode is: %v\n", errorCode)
	return errorCode
}

// GenerateRandomString - Generates a rand string of n - length
func GenerateRandomString(n int) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// DecryptString - ENCRYPTION AND DECRYPTION **/
// Takes two strings, cryptoText and keyString.
// cryptoText is the text to be decrypted and the keyString is the key to use for the decryption.
// The function will output the resulting plain text string with an error variable.
func DecryptString(cryptoText string, keyString string) (plainTextString string, err error) {

	// Format the keyString so that it's 32 bytes.
	newKeyString, err := hashTo32Bytes(keyString)

	// Encode the cryptoText to base 64.
	cipherText, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher([]byte(newKeyString))

	if err != nil {
		panic(err)
	}

	if len(cipherText) < aes.BlockSize {
		panic("cipherText too short")
	}

	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	stream.XORKeyStream(cipherText, cipherText)

	return string(cipherText), nil
}

// EncryptString - Takes two string, plainText and keyString.
// plainText is the text that needs to be encrypted by keyString.
// The function will output the resulting crypto text and an error variable.
func EncryptString(plainText string, keyString string) (cipherTextString string, err error) {

	// Format the keyString so that it's 32 bytes.
	newKeyString, err := hashTo32Bytes(keyString)

	if err != nil {
		return "", err
	}

	key := []byte(newKeyString)
	value := []byte(plainText)

	block, err := aes.NewCipher(key)

	if err != nil {
		panic(err)
	}

	cipherText := make([]byte, aes.BlockSize+len(value))

	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(cryptoRand.Reader, iv); err != nil {
		return
	}

	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(cipherText[aes.BlockSize:], value)

	return base64.URLEncoding.EncodeToString(cipherText), nil
}

// As we cannot use a variable length key, we must cut the users key
// up to or down to 32 bytes. To do this the function takes a hash
// of the key and cuts it down to 32 bytes.
func hashTo32Bytes(input string) (output string, err error) {

	if len(input) == 0 {
		return "", errors.New("No input supplied")
	}

	hasher := sha256.New()
	hasher.Write([]byte(input))

	stringToSHA256 := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	// Cut the length down to 32 bytes and return.
	return stringToSHA256[:32], nil
}

// DelaySeconds - delay for a number of seconds
func DelaySeconds(n time.Duration) {
	time.Sleep(n * time.Second)
}

// CheckError - check for err globally
func CheckError(err error, description string) {
	raven.SetDSN("https://4721f61661b945cd97dc2eeea0230658:6aecf01aeaa6470b91b7b9e4d9ffd9da@sentry.io/243952-STRUTSRAVEN")
	if err != nil {
		// panic(er) // CaptureErrorAndWait // CaptureError
		log.Println("Err ecountered on >>> ", description)
		log.Println("Encountered an Error >>> ", err.Error())
		// result := raven.CaptureError(err, nil)
		// log.Println("Go sentry >>> ", result)
		result := raven.CaptureErrorAndWait(err, nil)
		log.Println("Go sentry [2] >>> ", result)
	}
}

// GetAccountProvider - This case - for MTN and TIGO
func GetAccountProvider(account string) string {

	networkProvider := strings.HasPrefix(account, "250727") // true - tigo
	log.Println("Network Provider [2] >>> ", networkProvider)

	if networkProvider == true {
		return "TIGO"
	} else {
		return "MTN"
	}
}

// UniqueArr - return only the unique items in an arr
func UniqueArr(e []string) []string {
	r := []string{}

	for _, s := range e {
		if !ArrContains(r[:], s) {
			r = append(r, s)
		}
	}
	return r
}

// ArrContains - Check if above ArrContains
func ArrContains(e []string, c string) bool {
	for _, s := range e {
		if s == c {
			return true
		}
	}
	return false
}

// ConvertStringToInt - ConvertStringToInt
func ConvertStringToInt(str string) int {
	j, err := strconv.Atoi(str)
	if err != nil {
		log.Println("Error converting string id to int >>> ", j)
		return 0
	}
	return j
}

// ConvertIntToString - ConvertIntToString
func ConvertIntToString(num int) string {
	t := strconv.Itoa(num)
	return t
}

// FloatToString - FloatToString
func FloatToString(inputNum float64) string {
	// to convert a float number to a string
	// return strconv.FormatFloat(inputNum, 'f', 6, 64)
	return strconv.FormatFloat(inputNum, 'f', 0, 64)
}

// URLEncodeString encodes a string like Javascript's encodeURIComponent()
func URLEncodeString(str string) string {
	u, err := url.Parse(str)
	if err != nil {
		log.Println("Error encoding URL String >>> ", str)
		return ""
	}
	return u.String()
}

// Base64DecodeString - Base64DecodeString
func Base64DecodeString(str string) string {
	data, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		log.Println("error:", err)
		return ("")
	}
	// log.Printf("%q\n", data)
	strData := fmt.Sprintf("%s", data)
	return strData
}
